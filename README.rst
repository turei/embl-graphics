Materials for Graphics
######################

:contributions, issues: turei.denes@gmail.com

**Important:** install fonts before editing SVGs.

This is a collection of handy stuff for making presentations, posters and
figures. Some of these have been made according to the RWTH Aachen University
or EMBL style guidelines. See the
`corporate design guide at Heidelberg University`_,
`logo of University Heidelberg`_,
`corporate design guidelines (RWTH Aachen)`_,
`branding guidlines (EMBL-EBI)`_, `style guide (EMBL-EBI)`_,
and `guidelines and templates (EMBL HD)`_ for more details.

.. _`corporate design guide at Heidelberg University`: https://www.uni-heidelberg.de/university/corporatedesign/
.. _`logo of University Heidelberg`: https://www.uni-heidelberg.de/institutions/rectorate/kum/corporatedesign/logo.html
.. _`corporate design guidelines (RWTH Aachen)`: http://www.rwth-aachen.de/cms/root/Die-RWTH/Einrichtungen/Verwaltung/Stabsstellen/Marketing/~eqbm/Corporate-Design/?lidx=1
.. _`branding guidlines (EMBL-EBI)`: http://intranet.ebi.ac.uk/sites/intranet.ebi.ac.uk/files/content/outreach/EMBL_branding_guidelines.pdf
.. _`style guide (EMBL-EBI)`: http://intranet.ebi.ac.uk/outreach/writing-embl-style-guide
.. _`guidelines and templates (EMBL HD)`: https://intranet.embl.de/communication_outreach/templates/index.html

What to find here?
==================

embl.gpl
--------

Palette of official EMBL colors in ``gpl`` RGB format. Copy to
``~/.config/inkscape/palettes/`` to use it in Inkscape. GIMP is able to import
gpl palettes too. It has RGB 0-255 values in 3 columns, and color names. There
are 5 colors (green, blue, yellow, red, black), each in 7 tones, plus the
brown used often in titles on the webpage.

colors.py
---------

Some methods for reading and converting colors.

beamer_embl.tex
---------------

This is a presentation template for XeLaTeX and Beamer. Based on the PaloAlto
template.

embl1.pdf
---------

EMBL logo vector graphics.

embl1.svg
---------

EMBL logo vector graphics.

embl.svg
--------

EMBL logo with EMBL acronym.

fonts
-----

Without having these fonts, anything lacking embedded fonts (SVG and TeX)
won't work for you. See also the `icon fonts`_ from EMBL. Please note that
Helvetica Neue Linotype is owned by Adobe and licensed by EMBL. As an EMBL
member you are free to use it.

.. _`icon fonts`: http://intranet.ebi.ac.uk/outreach/fonts-and-icons

examples
--------

Here you find some of my works as examples. These contain many vector outlines
which are reusable elements for your own works. Please feel free to use. Just
open an SVG in Inkscape and copy-paste the shapes. Also if you edit SVGs
please first install the fonts because these are not embedded in these files.

aachen_graphics
---------------

Same materials for RWTH Aachen style. Color palettes, fonts, logos,
guidelines.

hd_graphics
-----------

Materials for Heidelberg University graphics.
