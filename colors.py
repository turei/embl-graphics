#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#
#  Copyright (c) 2014-2015 - EMBL-EBI
#
#  File author(s): Dénes Türei (denes@ebi.ac.uk)
#
#  Distributed under the GPLv3 License.
#  See accompanying file LICENSE.txt or copy at
#      http://www.gnu.org/licenses/gpl-3.0.html
#
#  Website: http://www.ebi.ac.uk/~denes
#

def embl_palette(inFile = 'embl_colors.tab'):
    cols = []
    with open(inFile, 'r') as f:
        series = []
        for i, l in enumerate(f):
            l = [x.strip() for x in l.split(',')]
            series.append(rgb2hex(tuple([256 * float(x) for x in l[0:3]])))
            if len(series) == 7:
                cols.append(series)
                series = []
    return cols

# color converting functions
def rgb2hex(rgb):
    return '#%02x%02x%02x' % rgb

def hex2rgb(self, rgbhex):
    rgbhex = rgbhex.lstrip('#')
    lv = len(rgbhex)
    return tuple(int(rgbhex[i:i + 2], 16) for i in range(0, lv, 2))

def rgb1(self, rgb256):
    return rgb256 if not any([i > 1 for i in rgb256]) \
        else tuple([x / float(255) for x in rgb256])

def rgb256(rgb1):
    return rgb1 if any([i > 1.0 for i in rgb1]) \
        else tuple([x * 255.0 for x in rgb1])